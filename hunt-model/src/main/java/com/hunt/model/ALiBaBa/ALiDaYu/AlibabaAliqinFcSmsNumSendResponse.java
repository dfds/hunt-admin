package com.hunt.model.ALiBaBa.ALiDaYu;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author ouyangan
 * @Date 2017/1/16/16:04
 * @Description
 */
public class AlibabaAliqinFcSmsNumSendResponse {
    private ALiDaYiSendMessageResult result;
    private String request_id;

    @Override
    public String toString() {
        return "AlibabaAliqinFcSmsNumSendResponse{" +
                "result=" + result +
                ", request_id='" + request_id + '\'' +
                '}';
    }

    public ALiDaYiSendMessageResult getResult() {
        return result;
    }

    public void setResult(ALiDaYiSendMessageResult result) {
        this.result = result;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
