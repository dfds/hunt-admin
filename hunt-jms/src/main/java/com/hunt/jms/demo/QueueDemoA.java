package com.hunt.jms.demo;

import com.hunt.service.SmsService;
import com.hunt.util.OathUtil;
import org.apache.activemq.broker.region.Destination;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.sql.Time;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author ouyangan
 * @Date 2017/1/17/12:25
 * @Description
 */
public class QueueDemoA implements MessageListener {
    //计数器
    private AtomicInteger count = new AtomicInteger(1);

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println();
            Thread.currentThread().sleep(2000L);
            ActiveMQTextMessage message1 = (ActiveMQTextMessage) message;
            System.out.println("队列接收消息-> " +message1.getText());
        } catch (InterruptedException | JMSException e) {
            e.printStackTrace();
        }
    }
}
